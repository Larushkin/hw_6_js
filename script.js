'use strict'
/**
 * Теорія 
 * 

1. Екранування допомагає нам вводити символи, які JS сприймає як спецсимволи


2.  "function declaration statement"
    1.Ім'я функції.
    2. Список параметрів.
    3. Інструкції, які будуть виконані після виклику функції.

    Функції типу "function definition expression"

    Така функція може бути анонімною (вона не має імені)
    Однак, ім'я може бути присвоєно для виклику самої себе всередині самої функції



3.  Це спосіб, згідно якого читається код JS.
    Код ми читаємо зверху вниз
     
 */




function createNewUser(){
    const name = prompt("Please, enter your name");
    const lastName = prompt("Please, enter your last name");
    const birthday = prompt("Please, enter date of birth (dd.mm.yyyy)");
    const newUser = {}
    Object.defineProperties(newUser, {
        name: {
            value: name,
            writable: false,
        },
        lastName: {
            value: lastName,
            writable: false,
        },
        getLogin: {
            value: function (){
                    let user = this.name[0] + this.lastName + '';
                    return user.toLowerCase();
                }
        },
        getAge: {
            value: function(){
                const now = new Date();
                const birthdayDate = birthday.split('.');
                const date = new Date(`${birthdayDate[1]}.${birthdayDate[0]}.${birthdayDate[2]}`);
                return new Date(Date.now() - date).getFullYear() - 1970;
            }
        },
        getPassword: {
            value: function(){
                return (name[0] + (lastName.toLowerCase()) + birthday.split('.')[2])
            }
        }

    })   
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
};

let user1 = createNewUser();
console.log(user1);



